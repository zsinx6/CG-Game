package core;

import br.usp.icmc.vicg.gl.core.Light;
import br.usp.icmc.vicg.gl.core.Material;
import br.usp.icmc.vicg.gl.jwavefront.JWavefrontObject;
import br.usp.icmc.vicg.gl.matrix.Matrix4;
import br.usp.icmc.vicg.gl.model.TextureRectangle;
import br.usp.icmc.vicg.gl.model.TextureSimpleModel;
import java.io.File;

import javax.media.Format;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.Player;
import javax.media.PlugInManager;
import javax.media.format.AudioFormat;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.media.opengl.GL;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;

import br.usp.icmc.vicg.gl.util.Shader;
import br.usp.icmc.vicg.gl.util.ShaderFactory;
import br.usp.icmc.vicg.gl.util.ShaderFactory.ShaderType;

import com.jogamp.opengl.util.AnimatorBase;
import com.jogamp.opengl.util.FPSAnimator;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Game extends KeyAdapter implements GLEventListener {

    private final Shader shader; // Gerenciador dos shaders
    //private final Shader shader2;
    private final Matrix4 modelMatrix;
    private final Matrix4 projectionMatrix;
    private final Matrix4 viewMatrix;
    private final JWavefrontObject model1;
    private final JWavefrontObject model3;
    Player player;
    //private final JWavefrontObject model2, model3, model4, model5, model6;
    private final JWavefrontObject[] model2;
    //private final Material material;
    private final Light light;
    private static GraphicsDevice device;
    //private final TextureSimpleModel rectangle;
    private float alpha;
    private float beta;
    private float delta;
    float accX;
    float aux[];
    float rot;
    float x[], y[], z[];
    float accY;
    float randx[], randy[];
    boolean jogo;
    float shipPositionX, shipPositionY;
    float velocidadeAsteroide;

    /*
     Toda a movimentação é feita em relação a nave, que fica SEMPRE em (0,0,0)
     e ela pode rotacionar em y e em x (para desviar dos meteoros), e os meteoros que se movem.
     Então é necessário rotacionar todo o sistema para deixar todos os objetos no sistema de coordenadas
     da nave.
     Um problema que não foi resolvido é que por algum motivo a luz está rodando junto com o meteoro
     e junto com a nave (ela deveria ficar parada). Uma possível solução, segundo o monitor, seria
     fazer a transformação oposta a feita nos objetos na luz (no setposition), para que quando as
     transformações fossem feitas, ela voltasse para a posição original (ex: 
     translada ela -2 antes, translada o objeto +2, as transformações se cancelam e ela volta para a posição
     incial).
     Falta tratar a colisão do meteoro com a nave, o que pode ser feito facilmente pois a nave
     está sempre em (0,0,0), só é preciso ajustar o raio de colisão para que fique visivelmente certo.
     Coisas a serem feitas é coloca um texto para dar a introdução ao jogo (Uma imagem com o nome do jogo
     pedindo para apertar enter para iniciar o jogo, ou algo do tipo), e uma tela de game over.
     Além de colocar algumas estrelas no céu ao fundo, essas estrelas também devem ser rotacionadas
     para que fiquem sempre na mesma posição (e dar a impressão de que a nave está se movendo pelo cenário)
    
    
     aaa
     */
    public Game() {
        // Carrega os shaders
        shader = ShaderFactory.getInstance(ShaderType.COMPLETE_SHADER);
        //shader2 = ShaderFactory.getInstance(ShaderType.TEXTURE_SHADER);
        modelMatrix = new Matrix4();
        projectionMatrix = new Matrix4();
        viewMatrix = new Matrix4();
        model2 = new JWavefrontObject[5];
        model3 = new JWavefrontObject(new File("./models/universo.obj"));
        //material = new Material();
        //rectangle = new TextureRectangle();
        //Carrega os modelos
        model1 = new JWavefrontObject(new File("./models/bmbcox.obj"));
        model2[0] = new JWavefrontObject(new File("./models/meteoro70.obj"));
        model2[1] = new JWavefrontObject(new File("./models/meteoro70.obj"));
        model2[2] = new JWavefrontObject(new File("./models/meteoro70.obj"));
        model2[3] = new JWavefrontObject(new File("./models/meteoro70.obj"));
        model2[4] = new JWavefrontObject(new File("./models/meteoro70.obj"));
        x = new float[5];
        y = new float[5];
        z = new float[5];
        randx = new float[5];
        randy = new float[5];
        aux = new float[5];

        light = new Light();

        alpha = 0;
        beta = 0;
        delta = 5;
        accX = 0;
        rot = 0;
        shipPositionX = 0;
        shipPositionY = 0;
        Random r = new Random();
        for (int i = 0; i < 5; i++) {
            x[i] = 0;
            y[i] = 0;
            z[i] = 0;
            randx[i] = 4 * r.nextFloat() - 2;
            randy[i] = 4 * r.nextFloat() - 2;
            aux[i] = 0;
        }
        accY = 0;
        jogo = true;
        velocidadeAsteroide = 0.2f;
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        GL3 gl = drawable.getGL().getGL3();
        Format input1 = new AudioFormat(AudioFormat.MPEGLAYER3);
        Format input2 = new AudioFormat(AudioFormat.MPEG);
        Format output = new AudioFormat(AudioFormat.LINEAR);
        PlugInManager.addPlugIn(
                "com.sun.media.codec.audio.mp3.JavaDecoder",
                new Format[]{input1, input2},
                new Format[]{output},
                PlugInManager.CODEC
        );
        try {
            player = Manager.createPlayer(new MediaLocator(new File("./models/song.mp3").toURI().toURL()));
            player.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        System.out.println("OpenGL Version: " + gl.glGetString(GL.GL_VERSION) + "\n");

        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl.glClearDepth(1.0f);

        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glEnable(GL.GL_CULL_FACE);

        shader.init(gl);
//        shader2.init(gl);
        //       shader2.bind();
        shader.bind();
        //     material.init(gl, shader2);
        modelMatrix.init(gl, shader.getUniformLocation("u_modelMatrix"));
        projectionMatrix.init(gl, shader.getUniformLocation("u_projectionMatrix"));
        viewMatrix.init(gl, shader.getUniformLocation("u_viewMatrix"));
        //   rectangle.init(gl, shader2);
        try {
            model1.init(gl, shader);
            model1.unitize();
            model1.dump();
            for (int i = 0; i < 5; i++) {
                model2[i].init(gl, shader);
                model2[i].unitize();
                model2[i].dump();
            }
            model3.init(gl, shader);
            model3.unitize();
            model3.dump();
        } catch (IOException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }

        //posiçao incial da luz
        light.setPosition(new float[]{0, 10, 10, 1.0f});
        light.setAmbientColor(new float[]{0.1f, 0.1f, 0.1f, 1.0f});
        light.setDiffuseColor(new float[]{0.75f, 0.75f, 0.75f, 1.0f});
        light.setSpecularColor(new float[]{0.7f, 0.7f, 0.7f, 1.0f});
        light.init(gl, shader);
        //rectangle.init(gl, shader);
        /*try {
         rectangle.loadTexture("models/face.jpg");
         } catch (IOException ex) {
         // Logger.getLogger(Example14.class.getName()).log(Level.SEVERE, null, ex);
         ex.printStackTrace();
         }*/
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        /* material.setAmbientColor(new float[]{0.0f, 0.0f, 1.0f, 0.25f});
         material.setDiffuseColor(new float[]{0.0f, 0.0f, 1.0f, 0.25f});
         material.setSpecularColor(new float[]{0.9f, 0.9f, 0.9f, 0.25f});
         material.setSpecularExponent(32);
         material.bind();
         */
        velocidadeAsteroide += 0.0005f;
        aux[0] = aux[0] + velocidadeAsteroide; //velocidade dos meteoros, "velocidade da nave"

        //aux[1] = aux[1] + 0.12f;
        //aux[2] = aux[2] + 0.09f;
        //aux[3] = aux[3] + 0.11f;
        //aux[4] = aux[4] + 0.05f;
        rot = rot + 0.5f; //velocidade de rotação do meteoro
        GL3 gl = drawable.getGL().getGL3();
        light.bind();

        gl.glClear(GL3.GL_COLOR_BUFFER_BIT | GL3.GL_DEPTH_BUFFER_BIT);
        viewMatrix.loadIdentity();
        viewMatrix.lookAt( //seta a posição do lookat para olhar sempre para a parte de trás da nave
                //(float) (2 * Math.sin(Math.toRadians(-pos))), (float) (2*Math.sin(Math.toRadians(pos2))), (float) ( 2* Math.cos(Math.toRadians(pos))) + (float) (2*Math.sin(Math.toRadians(pos2))),
                //(float) (2 * Math.sin(Math.toRadians(-pos))), 0, (float) ( 2* Math.cos(Math.toRadians(pos))),
                0, 0f, 5,
                0, 0, 0,
                0, 1, 0);
        viewMatrix.bind();
        projectionMatrix.loadIdentity();

        projectionMatrix.ortho( //a nave é usada ortogonal, pois não se move (Oz)
                -1f, 1f,
                -1f, 1f,
                -2, 2);
        projectionMatrix.bind();
        modelMatrix.loadIdentity();
        //modelMatrix.translate(0, 0, -1);
        modelMatrix.bind();
        //      rectangle.bind();
        //    rectangle.draw();

        projectionMatrix.loadIdentity();
        projectionMatrix.perspective(45f, 1f, 0.1f, 50f);
        projectionMatrix.bind();

        //Coloca a nave na origem, e rotaciona para o lado certo (posição incial)
        //pos e pos2 é a variável que controla a rotação (setinha),
        modelMatrix.loadIdentity();
        //modelMatrix.rotate(-pos2, 1, 0, 0);
        // modelMatrix.rotate(-pos, 0, 1, 0);
        float a = 1.5f;
        float ay = 1.5f;
        if (shipPositionX >= a) {
            accX = -accX;
        }
        if (shipPositionX <= -a) {
            accX = -accX;
        }
        if (shipPositionY >= ay) {
            accY = -accY;
        }
        if (shipPositionY <= -ay) {
            accY = -accY;
        }
        if (shipPositionX <= -a && accX > 0) {
            shipPositionX += accX;
        }
        if (shipPositionX >= a && accX < 0) {
            shipPositionX += accX;
        }
        if (shipPositionY <= -ay && accY < 0) {
            shipPositionY += -accY;
        }
        if (shipPositionY >= ay && accY > 0) {
            shipPositionY += -accY;
        }
        if (shipPositionX > -a && shipPositionX < a) {
            shipPositionX += accX;
        }
        if (shipPositionY > -ay && shipPositionY < ay) {
            shipPositionY += -accY;
        }

        //System.out.println(shipPositionX + "\t" + shipPositionY);
        modelMatrix.translate(shipPositionX, shipPositionY, 0);
        modelMatrix.rotate(-180, 0, 1.0f, 0);
        modelMatrix.rotate(-10, 1.0f, 0, 0);
        modelMatrix.bind();

        if (jogo) {
            model1.draw();
        }

        modelMatrix.loadIdentity();
        modelMatrix.translate(0, 0, 5);
        modelMatrix.bind();
        model3.draw();
        //para o segundo modelo:
        //meteoro é usado perspectiva para ele começar pequeno e ir aumentando
        //projectionMatrix.loadIdentity();
        //projectionMatrix.perspective(45f, 1f, 0.1f, 50f);
        //projectionMatrix.bind();
        modelMatrix.loadIdentity();

        //mudança de coordenadas para deixar o meteoro sempre no sistema da nave
        for (int i = 0; i < 1 && jogo; i++) {

            z[i] = aux[i];
            //System.out.println(z[i]);
            if (z[i] - 50 >= 15) { //se ele saiu da tela, volta para o começo
                //e calcula a posição aleatória para inicio
                aux[i] = 0;
                Random r = new Random();
                randx[i] = 4 * r.nextFloat() - 2;
                randy[i] = 4 * r.nextFloat() - 2;
                // z[i] = -20;
            }
            float distX = randx[i] - shipPositionX;
            float distY = randy[i] - shipPositionY;
            float distZ = z[i] - 50;
            double in = Math.pow(distX, 2) + Math.pow(distY, 2) + Math.pow(distZ, 2);
            float G = 0.04f;
            if (distX < -0.7f && accX < 1.2) {
                accX += (float) -(G / Math.pow(in, 3));
            } else if (distX > 0.7 && accX > -1.2) {
                accX += (float) (G / Math.pow(in, 3));
            }
            if (distY < -0.7f && accY < 1.2) {
                accY += (float) (G / Math.pow(in, 3));
            } else if (distY > 0.7 && accY > -1.2) {
                accY += (float) -(G / Math.pow(in, 3));
            }

            /*
             if (distX > 0) {
             if (accX < 1 && accX > -1) {
             accX += (float) (0.1 / (Math.pow(distX + 3, 3) * (Math.pow(-distZ, 3) + 16)));
             }
             System.out.println(accX);

             } else {
             if (accX < 1 && accX > -1) {
             accX += (float) (0.1 / (Math.pow(distX - 3, 3) * (Math.pow(-distZ, 3) + 16)));
             }
             }
             if (distY < 0) {
             if (accY < 1 && accY > -1) {
             accY += (float) (0.1 / (Math.pow(distY + 3, 3) * (Math.pow(-distZ, 3) + 16)));
             }
             System.out.println(accX);

             } else {
             if (accY < 1 && accY > -1) {
             accY += (float) (0.1 / (Math.pow(distY - 3, 3) * (Math.pow(-distZ, 3) + 16)));
             }
             }
             if (shipPositionX > 10 || shipPositionX < -10 || shipPositionY > 10 || shipPositionY < -10) {
             //   shipPositionX = 0;
             // shipPositionY = 0;
             accX = 0;
             accY = 0;
             }
             System.out.println(accY);
             */
            //System.out.println("randx: " + randx[i] + "\t" + "randy:" + randy[i] + "\n positionX: " + 
            //       (randx[i] - shipPositionX) + " positionY:" + (randy[i] - shipPositionY));
            //System.out.println("posx: " + shipPositionX + "\tposy: "+shipPositionY);
            if (in < 0.5f) {
                jogo = false;
                System.out.println("Fim de jogo");
            }
            if (aux[i] > 0) {
                modelMatrix.translate(x[i], y[i], z[i]);
            }
            //System.out.println(z[i]);
            modelMatrix.translate(randx[i], randy[i], -50f);
            modelMatrix.rotate(rot, 0, 1, 0);

            modelMatrix.bind();
            if (jogo) {
                model2[i].draw();
            }
            if (!jogo) {
                player.stop();
                try {
                    player = Manager.createPlayer(new MediaLocator(new File("./models/ex.mp3").toURI().toURL()));
                    player.start();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }

        // Força execução das operações declaradas
        gl.glFlush();
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        model1.dispose();
        for (int i = 0; i < 5; i++) {
            model2[i].dispose();
        }
        model3.dispose();
        //  rectangle.dispose();
    }

    @Override
    public void keyPressed(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_PAGE_UP://faz zoom-in
                delta = delta * 0.809f;
                accX = 0;
                accY = 0;
                break;
            case KeyEvent.VK_PAGE_DOWN://faz zoom-out
                delta = delta * 1.1f;
                break;
            case KeyEvent.VK_UP://gira sobre o eixo-x
                alpha = alpha - 5;
                if (shipPositionY != -0.2f) { //esses controles são para não permitir que a nave volte para trás
                    //ou fique muita distorcida
                    accY = accY - 0.003f;
                }
                break;
            case KeyEvent.VK_DOWN://gira sobre o eixo-x
                alpha = alpha + 5;
                if (shipPositionY != 0.2f) {
                    accY = accY + 0.003f;
                }
                break;
            case KeyEvent.VK_LEFT://gira sobre o eixo-y
                beta = beta - 5;
                if (shipPositionX != -0.2f) {
                    accX = accX - 0.003f;
                }
                break;
            case KeyEvent.VK_RIGHT://gira sobre o eixo-y
                beta = beta + 5;
                if (shipPositionX != 0.2f) {
                    accX = accX + 0.003f;
                }
                break;
        }
    }

    public static void main(String[] args) {
        // Get GL3 profile (to work with OpenGL 4.0)
        GLProfile profile = GLProfile.get(GLProfile.GL3);

        // Configurations
        GLCapabilities glcaps = new GLCapabilities(profile);
        glcaps.setDoubleBuffered(true);
        glcaps.setHardwareAccelerated(true);

        // Create canvas
        GLCanvas glCanvas = new GLCanvas(glcaps);

        // Add listener to panel
        Game listener = new Game();
        glCanvas.addGLEventListener(listener);
        //GraphicsEnvironment environment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        //device = environment.getDefaultScreenDevice();
        Frame frame = new Frame("Asteróides");
        frame.setSize(600, 600);
        //frame.setUndecorated(true);
        //frame.setResizable(false);
        //device.setFullScreenWindow(frame);
        frame.add(glCanvas);
        frame.addKeyListener(listener);
        final AnimatorBase animator = new FPSAnimator(glCanvas, 60);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        frame.setVisible(true);
        animator.start();
    }
}
